<?php

namespace Controllers;

use Resources,
    Models;

class LOGIN extends Resources\Controller {

    public function __construct() {
        parent::__construct();
        $fungsi = new \Libraries\Fungsi();
        $request = file_get_contents('php://input');
        $jreq = json_decode(preg_replace('/[^a-zA-Z0-9\-\_\#\@\ \.\,\:\"\]\[\}\{]/', '', $request));
        $this->request = $request;
        $this->tipe = strtoupper($jreq->tipe);

        $this->ip = getenv('REMOTE_ADDR');
        $this->website = $jreq->website;
        $req_noid = (isset($jreq->noid)) ? $jreq->noid : '';
        $this->noid = strtoupper($req_noid);
        $req_username = (isset($jreq->username)) ? $jreq->username : '';
        $this->username = strtoupper($req_username);
        $req_password = (isset($jreq->password)) ? $jreq->password : '';
        $this->pwd = strtoupper($req_password);
        $this->token = strtoupper($jreq->token);

        $tipebrowser = getenv('HTTP_USER_AGENT') . getenv('HTTP_ACCEPT_LANGUAGE');
        if (strlen($jreq->appid) < 10 && $jreq->tipe == 'LOGIN') {
            $appid_browser = $fungsi->randomString(16);
        } else {
            $appid_browser = $jreq->appid;
        }

        $this->appid = preg_replace("/[^a-zA-Z0-9]/", "", $appid_browser);
        $clientData = $this->appid;
        $secClient = substr(base64_encode($appid_browser . $tipebrowser), 15, 50);

        $this->browserid = $secClient;
    }

    public function CEKUSERNAME($interface) { //bisa juga untuk cek device android apakah terdaftar
        $db = new Models\Databases();
        $reply = new Models\LoginRespon();

        $reply->tipe = 'CEKUSERNAME';
        $reply->interface = $interface;
        $reply->appid = $this->appid;
        $data_appid = $this->browserid . '#' . $this->appid;

        $sql = "select alias from tbl_member_channel "
                . "where appid = '$data_appid' and interface = '$interface' "
                . "order by last_used desc limit 1;";

        $arr_cek = $db->singleRow($sql);
        if (isset($arr_cek->alias)) {
            $reply->status = "SUKSESCEKUSERNAME";
            $reply->username = $arr_cek->alias;
            $reply->message = 'cek username berhasil';
        } else {
            $reply->status = "GAGAL";
            $reply->message = 'username tidak tersedia';
        }
        echo json_encode($reply);
    }

    public function LOGIN($interface) {
        $db = new Models\Databases();
        $reply = new Models\LoginRespon();
        $fungsi = new \Libraries\Fungsi();
        $konfig = new \Libraries\Konfigurasi();

        $reply->tipe = 'LOGIN';
        $reply->interface = $interface;

        $sql = "select mc.noid, mc.nama, mb.nohp_email, mb.alamat, mb.saldo, status, passw, appid, token, mb.tipe, mb.jenis, mb.hak_saldo from tbl_member_channel mc "
                . "inner join (select noid,saldo,tipe,jenis,nama,nohp_email,alamat,hak_saldo from tbl_member_account) as mb on mb.noid = mc.noid "
                . "where mc.alias = '$this->username' and mc.interface = '$interface';";

        $arr_cek = $db->singleRow($sql);

        if (isset($arr_cek->noid)) {
            $noid = $arr_cek->noid;
            $nama = $arr_cek->nama;
            $nohp_email = $arr_cek->nohp_email;
            $alamat = $arr_cek->alamat;
            $jenis = $arr_cek->tipe;
            $jenis_member = $arr_cek->jenis;
            $hak_saldo = $arr_cek->hak_saldo;
            $saldo = $arr_cek->saldo;
            $status = $arr_cek->status;
            $passwd = $arr_cek->passw;
            $aplikasiid = $arr_cek->appid;
            $aplikasiid_add = $this->browserid . '#' . $this->appid;

            $hak_saldo_upline = 0;
            if($jenis == 'M3' || $jenis == 'M2'){
                $prefix_ca = substr($noid, 0, 3);
                $noid_ca = $prefix_ca.'0000000000000';
                $arr_upline = $db->cekNoidMember($noid_ca);
                $hak_saldo_upline = $arr_upline->hak_saldo;
            }
            //aturan login
            $jeniswebsite = $jenis . $this->website;
            $konfig->aturanLogin($jeniswebsite, $jenis, $this->ip);

            if ($status == 1) {
                if ($passwd == $this->pwd) {
                    if ($aplikasiid == $aplikasiid_add || $saldo <= 1000000) {
                        //login sukses
                        $reply->status = 'SUKSESLOGIN';
                        $reply->message = 'Berhasil Login';
                        $reply->noid = $noid;
                        $reply->username = $this->username;
                        $reply->token = $fungsi->randomString(20);
                        $reply->nama = $nama;
                        $reply->alamat = $alamat;
                        $reply->jenis = $jenis;
                        $reply->jenis_member = $jenis_member;
                        $reply->saldo = $fungsi->rupiah($saldo);
                        $reply->appid = $this->appid;
                        $reply->hak_saldo = $hak_saldo;
                        $reply->hak_saldo_upline = $hak_saldo_upline;
                        $strSqlUpLogin = "update tbl_member_channel set token = '$reply->token', last_used = now(), ip = '$this->ip', status = 1,"
                                . "appid = '$aplikasiid_add', salah_pin = 0 where noid = '$noid' and alias = '$this->username' and interface = '$interface'";
                        $db->singleRow($strSqlUpLogin);
                    } else {
                        //kirim otp
                        $reply->message = 'OTP';
                        $reply->appid = $aplikasiid_add;
                        $otp = $fungsi->randomString(6);
                        $strSqlUpAppid = "update tbl_member_channel set appid = '$aplikasiid_add', last_used = now(), token = '$otp', "
                                . "status = 2 where alias = '$this->username' and interface = '$interface'";
                        $db->singleRow($strSqlUpAppid);
                        $message = $konfig->namaAplikasi() . " One Time Password OTP=$otp untuk melanjutkan Login";
                        //kirim sms
                        $db->kirimMessage($noid, $message);
                    }
                } else {
                    //salah password, update salah password
                    $strSqlSp = "update tbl_member_channel set salah_pin = salah_pin + 1 "
                            . "where alias = '$this->username' and interface='$interface' returning salah_pin";
                    $arr_sp = $db->singleRow($strSqlSp);
                    $salah_pin = $arr_sp->salah_pin;
                    $reply->message = 'Gagal Salah Password ' . $salah_pin . ' kali';
                    if ($salah_pin > 4) {
                        $strSqlBlok = "update tbl_member_channel set status = 0, salah_pin = 0 where alias = '$this->username' and interface='$interface'";
                        $db->singleRow($strSqlBlok);
                        $reply->message = 'Akun anda telah terblokir, silahkan mengulang registrasi aplikasi';
                    }
                }
            } elseif ($status == 2) {
                if ($passwd == $this->pwd) {
                    if ($this->token == $arr_cek->token) {
                        //login sukses
                        $reply->status = 'SUKSESLOGIN';
                        $reply->message = 'Berhasil Login';
                        $reply->noid = $noid;
                        $reply->username = $this->username;
                        $reply->token = $fungsi->randomString(20);
                        $reply->nama = $nama;
                        $reply->alamat = $alamat;
                        $reply->jenis = $jenis;
                        $reply->jenis_member = $jenis_member;
                        $reply->saldo = $fungsi->rupiah($saldo);
                        $reply->appid = $this->appid;
                        $reply->hak_saldo = $hak_saldo;
                        $reply->hak_saldo_upline = $hak_saldo_upline;
                        $strSqlUpLogin = "update tbl_member_channel set token = '$reply->token', last_used = now(), ip = '$this->ip', status = 1,"
                                . "appid = '$aplikasiid_add', salah_pin = 0 where noid = '$noid' and alias = '$this->username' and interface = '$interface'";
                        $db->singleRow($strSqlUpLogin);
                    } else {
                        //kirim otp
                        $reply->message = 'OTP';
                        $otp = $fungsi->randomString(6);
                        $strSqlUpAppid = "update tbl_member_channel set appid = '$aplikasiid_add', last_used = now(), token = '$otp', "
                                . "status = 2 where alias = '$this->username' and interface = '$interface'";
                        $db->singleRow($strSqlUpAppid);
                        $message = $konfig->namaAplikasi() . " One Time Password OTP=$otp untuk melanjutkan Login";
                        //kirim sms
                        $db->kirimMessage($noid, $message);
                    }
                } else {
                    //salah password, update salah password
                    $strSqlSp = "update tbl_member_channel set salah_pin = salah_pin + 1 "
                            . "where alias = '$this->username' and interface='$interface' returning salah_pin";
                    $arr_sp = $db->singleRow($strSqlSp);
                    $salah_pin = $arr_sp->salah_pin;
                    $reply->message = 'Gagal Salah Password ' . $salah_pin . ' kali';
                    if ($salah_pin > 4) {
                        $strSqlBlok = "update tbl_member_channel set status = 0, salah_pin = 0 where alias = '$this->username' and interface='$interface'";
                        $db->singleRow($strSqlBlok);
                        $reply->message = 'Akun anda telah terblokir, silahkan mengulang registrasi aplikasi';
                    }
                }
            } else {
                $reply->message = 'Akun anda diblokir. Silahkan menghubungi Customer Service kami melalui live chat';
            }
        } else {
            $reply->message = 'Username tidak tersedia';
        }
        echo json_encode($reply);
    }

    public function CEKSESSION($interface) {
        $db = new Models\Databases();
        $reply = new Models\LoginRespon();
        $fungsi = new \Libraries\Fungsi();
        $aplikasiid_add = $this->browserid . '#' . $this->appid;

        $reply->tipe = 'CEKSESSION';
        $reply->interface = $interface;

        $sql = "select mc.alias,mb.nama,mb.saldo,mb.tipe,mb.jenis,mb.hak_saldo,mc.noid from tbl_member_channel mc "
                . "inner join (select noid,nama,saldo,tipe,jenis,hak_saldo from tbl_member_account) as mb on mb.noid = mc.noid "
                . "where mc.appid = '$aplikasiid_add' and mc.token = '$this->token' "
                . "and mc.noid = '$this->noid' and mc.interface = '$interface' and mc.alias = '$this->username' "
                . "and mc.last_used > now() - interval '60 minutes';";
        $arr_cek = $db->singleRow($sql);

        if (isset($arr_cek->alias)) {
            $noid = $arr_cek->noid;
            $jenis = $arr_cek->tipe;
            $hak_saldo_upline = 0;
            if($jenis == 'M3' || $jenis == 'M2'){
                $prefix_ca = substr($noid, 0, 3);
                $noid_ca = $prefix_ca.'0000000000000';
                $arr_upline = $db->cekNoidMember($noid_ca);
                $hak_saldo_upline = $arr_upline->hak_saldo;
            }
            
            $jeniswebsite = $arr_cek->tipe . $this->website;
            $reply->status = "SUKSESCEKSESSION";
            $reply->nama = $arr_cek->nama;
            $reply->jenis = $arr_cek->tipe;
            $reply->jenis_member = $arr_cek->jenis;
            $reply->saldo = $fungsi->rupiah($arr_cek->saldo);
            $reply->message = 'Session Login masih Aktif';
            $reply->hak_saldo = $arr_cek->hak_saldo;
            $reply->hak_saldo_upline = $hak_saldo_upline;
        } else {
            $reply->response_code = '0099';
            $reply->response_message = 'Session Telah Habis';
            $reply->message = 'Session Login telah Habis';
        }
        echo json_encode($reply);
    }

    public function LOGOUT($interface) {
        $db = new Models\Databases();
        $reply = new Models\LoginRespon();
        $aplikasiid_add = $this->browserid . '#' . $this->appid;
        $sql = "update tbl_member_channel set token = '', last_used = '2015-01-01 01:01:01', ip = '' "
                . "where alias = '$this->username' and appid = '$aplikasiid_add' and token = '$this->token' and interface = '$interface';";
        $db->singleRow($sql);

        $reply->status = 'SUKSESLOGOUT';
        $reply->tipe = 'LOGOUT';
        $reply->interface = $interface;
        echo json_encode($reply);
    }

}
