<?php

switch ($action_request) {
    case "add":

        $interface_add = strtoupper(trim($jreq->detail->interface));
        $ip_add = '';

        if ($interface_add == 'NFC') {
            $noid_add = strtoupper(trim($jreq->detail->noid));
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = strtoupper(trim($jreq->detail->alias));
            $passw_add = strtoupper(trim($jreq->detail->passw));
            $limit_trx_add = trim($jreq->detail->limit_trx);
            $msg_out = "Registrasi Kartu NFC " . $konfig->namaAplikasi() . " telah berhasil. UID:$alias_add a.n $nama_add "
                    . "PIN Transaksi=$passw_add dengan limit transaksi per hari $limit_trx_add";
            if (($noid_add == $noid) || (substr($noid_add, 0, 7) == substr($noid, 0, 7) && $tipe_member == 'M2')) {
                
            } else {
                $error->regAccountTidakBerhak($saldo_member, $file_request);
            }
            $nohp_email_member = '';
        } elseif ($interface_add == 'WEB') {
            $noid_add = $noid;
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = substr(str_replace(' ', '', $nama_add), 0, 6) . $fungsi->randomNumber(4);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = 0;
            $msg_out = "Registrasi Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                    . "Website " . $konfig->urlWebLogin() . " USERNAME=$alias_add PASSWORD=$passw_add";
        } elseif ($interface_add == 'H2H') {
            $noid_add = $noid;
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $ip_add = strtoupper(trim($jreq->detail->ip));
            $alias_add = substr(str_replace(' ', '', $nama_add), 0, 6) . $fungsi->randomNumber(4);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = 0;
            $token_add = $fungsi->randomString(20);
            $appid_add = $fungsi->randomString(20);
            $msg_out = "Registrasi Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                    . "CREDENTIAL H2H USERNAME=$alias_add TOKEN=$token_add APPID=$appid_add";
        } elseif ($interface_add == 'PINPAY') {
            $noid_add = $noid;
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = $fungsi->randomString(3) . $fungsi->randomNumber(3);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = trim($jreq->detail->limit_trx);
            $msg_out = "Registrasi Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. PINPAY=$alias_add a.n $nama_add "
                    . "PIN Transaksi=$passw_add dengan limit transaksi per hari $limit_trx_add";
        } else {
            //die;
        }

        $arr_cek_alias = $db->cekAliasChannel($alias_add);

        if (!isset($arr_cek_alias->id)) {

            $sql = "BEGIN TRANSACTION;"
                    . "insert into tbl_member_channel (interface,noid,alias,reg_date,last_used,passw,nama,email,limit_trx) values "
                    . "('$interface_add','$noid_add','$alias_add',now(),now(),'$passw_add','$nama_add','$nohp_email_member',$limit_trx_add);"
                    . "COMMIT;";

            $db->singleRow($sql);
            $db->kirimMessage($noid_add, $msg_out);
            $response = array(
                'response_code' => '0000',
                'response_message' => "Sukses Registrasi $interface_add $nama_add"
            );
        } else {
            $error->regAccountAliasTerdaftar($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
    case "block":

//edit nama email
        $id_add = strtoupper(trim($jreq->detail->id));

        $arr = $db->cekIdChannel($id_add);

        if (isset($arr->id)) {
            $noid_add = $arr->noid;

            if ($arr->status == 1) {
                $stat = 0;
                $action_add = 'BLOKIR';
            } else {
                $stat = 1;
                $action_add = 'BUKA BLOKIR';
            }

            $aturan = 1; //pegawai boleh edit semua nya, M1 edit downline, M2 edit downline, M3 edit diri sendiri
            if ($aturan == 1) {

                $sql = "BEGIN TRANSACTION;"
                        . "update tbl_member_channel set status = $stat "
                        . "where id = $id_add;"
                        . "COMMIT;";
                $db->singleRow($sql);

                $response = array(
                    'response_code' => '0000',
                    'response_message' => "$action_add $noid_add BERHASIL",
                    'saldo' => $saldo_member
                );
            } else {
                $error->regAccountTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->accountTidakAda($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
    case "check":

        $dest_add = strtoupper($jreq->detail->alias);
        $arr = $db->cekAliasChannel($dest_add);
        if (isset($arr->id)) {
            $response = array(
                'response_code' => '0000',
                'response_message' => 'CEK DATA MEMBER CHANNEL BERHASIL',
                'id' => $arr->id,
                'interface' => $arr->interface,
                'noid' => $arr->noid,
                'alias' => $arr->alias,
                'nama' => $arr->nama,
                'email' => $arr->email,
                'limit_trx' => $arr->limit_trx,
                'today_trx' => $arr->today_trx
            );
        } else {
            $response = array(
                'response_code' => '0501',
                'response_message' => 'ACCOUNT ' . $dest_add . ' TIDAK DITEMUKAN',
                'saldo' => $saldo_member);
        }

        $reply = json_encode($response);
        break;
    case "check_nfc":

        $dest_add = strtoupper($jreq->detail->alias);
        $arr = $db->cekAliasChannel($dest_add);
        if (isset($arr->id)) {
            $response = array(
                'response_code' => '0000',
                'response_message' => 'CEK DATA MEMBER CHANNEL BERHASIL',
                'id' => $arr->id,
                'interface' => $arr->interface,
                'noid' => $arr->noid,
                'alias' => $arr->alias,
                'nama' => $arr->nama,
                'email' => $arr->email,
                'limit_trx' => $arr->limit_trx,
                'today_trx' => $arr->today_trx,
                "photo" => $arr->photo_identity
            );
        } else {
            $response = array(
                'response_code' => '0501',
                'response_message' => 'ACCOUNT ' . $dest_add . ' TIDAK DITEMUKAN',
                'saldo' => $saldo_member);
        }

        $reply = json_encode($response);
        break;
    case "delete":

        $alias_add = strtoupper(trim($jreq->detail->alias));
        $arr = $db->cekAliasChannel($alias_add);
        if (isset($arr->id)) {
            $noid_add = $arr->noid;
            if ($arr->interface == 'MOBILE') {
                if (($noid_add == $noid) || (substr($noid_add, 0, 7) == substr($noid, 0, 7) && $tipe_member == 'M2')) {
                    
                } else {
                    $error->globalTidakBerhak($saldo_member, $file_request);
                }
            }

            if ($jenis_member == '2' || $noid_add == $noid || $arr->interface == 'MOBILE') {

                $sql = "BEGIN TRANSACTION;"
                        . "delete from tbl_member_channel where id = $arr->id;"
                        . "COMMIT;";
                unlink('../../../public/photoIdentity/' . $arr->photo_identity);
                $db->singleRow($sql);
                $msg_out = "Channel $interface $arr->alias a.n $arr->nama TELAH DIHAPUS";
                $db->kirimMessage($noid_add, $msg_out);
                $response = array(
                    'response_code' => '0000',
                    'response_message' => 'HAPUS DATA BERHASIL',
                    'saldo' => $saldo_member
                );
            } else {
                $error->globalTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->channelTidakAda($saldo_member, $file_request);
        }
        $reply = json_encode($response);
        break;
    case "delete_nfc":

        $alias_add = strtoupper(trim($jreq->detail->alias));
        $arr = $db->cekAliasChannel($alias_add);
        if (isset($arr->id)) {
            $noid_add = $arr->noid;
            if ($arr->interface == 'MOBILE') {
                if (($noid_add == $noid) || (substr($noid_add, 0, 7) == substr($noid, 0, 7) && $tipe_member == 'M2')) {
                    
                } else {
                    $error->globalTidakBerhak($saldo_member, $file_request);
                }
            }

            if ($jenis_member == '2' || $noid_add == $noid || $arr->interface == 'MOBILE') {
                $sql = "BEGIN TRANSACTION;"
                        . "delete from tbl_member_channel where id = $arr->id;"
                        . "COMMIT;";
                $db->singleRow($sql);
                $msg_out = "Channel $interface $arr->alias a.n $arr->nama TELAH DIHAPUS";
                $db->kirimMessage($noid_add, $msg_out);
                $response = array(
                    'response_code' => '0000',
                    'response_message' => 'DELETE DATA BERHASIL',
                    'saldo' => $saldo_member
                );
            } else {
                $error->globalTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->channelTidakAda($saldo_member, $file_request);
        }
        $reply = json_encode($response);
        break;
    case "edit":

        $id_add = strtoupper(trim($jreq->detail->id));
        $arr = $db->cekIdChannel($id_add);
        $interface_add = $arr->interface;
        $ip_add = '';

        if ($interface_add == 'NFC') {
            $noid_add = $arr->noid;
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = strtoupper(trim($jreq->detail->alias));
            $passw_add = trim($jreq->detail->passw);
            $limit_trx_add = trim($jreq->detail->limit_trx);
            $msg_out = "Update Kartu NFC " . $konfig->namaAplikasi() . " telah berhasil. UID:$alias_add a.n $nama_add "
                    . "PIN Transaksi=$passw_add dengan limit transaksi per hari $limit_trx_add";
            if (($noid_add == $noid) || (substr($noid_add, 0, 7) == substr($noid, 0, 7) && $tipe_member == 'M2')) {
                
            } else {
                $error->regAccountTidakBerhak($saldo_member, $file_request);
            }
            $nohp_email_member = '';
        } elseif ($interface_add == 'WEB') {
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = substr(str_replace(' ', '', $nama_add), 0, 6) . $fungsi->randomNumber(4);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = 0;
            $msg_out = "Update Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                    . "Website " . $konfig->urlWebLogin() . " USERNAME=$alias_add PASSWORD=$passw_add";
        } elseif ($interface_add == 'H2H') {
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $ip_add = strtoupper(trim($jreq->detail->ip));
            $alias_add = substr(str_replace(' ', '', $nama_add), 0, 6) . $fungsi->randomNumber(4);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = 0;
            $token_add = $fungsi->randomString(20);
            $appid_add = $fungsi->randomString(20);
            $msg_out = "Update Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                    . "CREDENTIAL H2H USERNAME=$alias_add TOKEN=$token_add APPID=$appid_add";
        } elseif ($interface_add == 'PINPAY') {
            $nama_add = strtoupper(trim($jreq->detail->nama));
            $alias_add = $fungsi->randomString(3) . $fungsi->randomNumber(3);
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = trim($jreq->detail->limit_trx);
            $msg_out = "Update Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. PINPAY=$alias_add a.n $nama_add "
                    . "PIN Transaksi=$passw_add dengan limit transaksi per hari $limit_trx_add";
        } else {
            //die;
        }

        $arr_cek_alias = $db->cekAliasChannel($alias_add);

        if (!isset($arr_cek_alias->id)) {
            if ($jenis_member == '2' || $noid_add == $noid || $interface_add == 'NFC') {
                $sql = "BEGIN TRANSACTION;"
                        . "update tbl_member_channel set alias='$alias_add',passw='$passw_add',nama='$nama_add',status=1,"
                        . "limit_trx=$limit_trx_add "
                        . "where id = $id_add;"
                        . "COMMIT;";

                $db->singleRow($sql);
                $db->kirimMessage($noid_add, $msg_out);
                $response = array(
                    'response_code' => '0000',
                    'response_message' => "Sukses Update $interface_add $nama_add",
                    'saldo' => $saldo_member
                );
            } else {
                $error->globalTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->regAccountAliasTerdaftar($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
    case "reset":

        $id_add = strtoupper(trim($jreq->detail->id));
        $arr = $db->cekIdChannel($id_add);
        $interface_add = $arr->interface;
        $ip_add = '';

        if ($interface_add == 'WEB') {
            $alias_add = $arr->alias;
            $noid_add = $arr->noid;
            $passw_add = $fungsi->randomString(6);
            $limit_trx_add = 0;
            $msg_out = "Reset Akun $interface_add " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                    . "Website " . $konfig->urlWebLogin() . " USERNAME=$alias_add PASSWORD=$passw_add";
        } else {
            $error->resetPasswordGagal($saldo_member, $file_request);
        }

        if ($jenis_member == '2' || $noid_add == $noid) {
            $sql = "BEGIN TRANSACTION;"
                    . "update tbl_member_channel set passw='$passw_add',status=1,"
                    . "limit_trx=$limit_trx_add, salah_pin = 0 "
                    . "where id = $id_add;"
                    . "COMMIT;";

            $db->singleRow($sql);
            $db->kirimMessage($noid_add, $msg_out);
            $response = array(
                'response_code' => '0000',
                'response_message' => "Sukses Reset Password $interface_add $alias_add",
                'saldo' => $saldo_member
            );
        } else {
            $error->globalTidakBerhak($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
}
