<?php

switch ($action_request) {
    case "add":
        $nama_add = strtoupper(trim($jreq->detail->nama));
        $nik_add = strtoupper(trim($jreq->detail->nik));
        $tgl_lahir_add = strtoupper(trim($jreq->detail->tgl_lahir));
        $alamat_add = strtoupper(trim($jreq->detail->alamat));
        $provinsi = strtoupper($jreq->detail->provinsi);
        $provinsi_value = strtoupper($jreq->detail->provinsi_value);
        $kota_kabupaten = strtoupper($jreq->detail->kota_kabupaten);
        $kota_kabupaten_value = strtoupper($jreq->detail->kota_kabupaten_value);
        $kecamatan = strtoupper($jreq->detail->kecamatan);
        $kecamatan_value = strtoupper($jreq->detail->kecamatan_value);
        $kelurahan = strtoupper($jreq->detail->kelurahan);
        $kelurahan_value = strtoupper($jreq->detail->kelurahan_value);
        $rt = strtoupper($jreq->detail->rt);
        $rw = strtoupper($jreq->detail->rw);
        $kodepos = strtoupper($jreq->detail->kodepos);
        $nohp_email_add = strtoupper($jreq->detail->nohp_email);
        $jenis = strtoupper($jreq->detail->jenis);
        $tipe = strtoupper($jreq->detail->tipe);

        $prefix_ca = '0';
        $prefix_subca = '0';
        $noid_ca = '0';
        $noid_subca = '0';
        $hak_saldo = 1;

        $aturan = $tipe_member . $tipe; //ADMIN... ADMINMEMBER SALESMEMBER
        if ($tipe_member == 'ADMIN' || $aturan == 'SALESM1' || $aturan == 'M1M2' || $aturan == 'M2M3') {
            if ($jenis == 'PEGAWAI') {
                $jenis_tbl = 2;
                switch ($tipe) {
                    case "ADMIN":
                        $prefix_tipe = '000000000001';
                        break;
                    case "FINANCE":
                        $prefix_tipe = '000000000002';
                        break;
                    case "CS":
                        $prefix_tipe = '000000000003';
                        break;
                    case "SALES":
                        $prefix_tipe = '000000000004';
                        break;
                    case "REPORTER":
                        $prefix_tipe = '000000000005';
                        break;
                }

                $sql_count = "select count(id) as jml from tbl_member_account where tipe = '$tipe'";
                $arr_count = $db->singleRow($sql_count);
                $ittr = str_pad($arr_count->jml + 1, 4, "0", STR_PAD_LEFT);
                $noid_add = $prefix_tipe . $ittr;

                if (is_numeric($nohp_email_add) !== TRUE) {
                    $tipe_message = 'EMAIL';
                } else {
                    $error->regAccountHarusEmail($saldo_member, $file_request);
                }
            } elseif ($jenis == 'MEMBER') {
                $jenis_tbl = 1;
                if ($jreq->detail->tipe == 'M1' && ($aturan == 'SALESM1' || $aturan == 'ADMINM1')) {
                    $sql_count = "select count(id) as jml from tbl_member_account where tipe = '$tipe'";
                    $arr_count = $db->singleRow($sql_count);
                    $prefix_ca = str_pad($arr_count->jml + 1, 3, "0", STR_PAD_LEFT);
                    $noid_add = $prefix_ca . '0000' . '000000000';
                    $noid_ca = $noid_add;
                    $noid_subca = $noid_add;
                    $hak_saldo = isset($jreq->detail->korwil) ? (int) $jreq->detail->korwil : 1;
                } elseif ($jreq->detail->tipe == 'M2' && $aturan == 'M1M2') {
                    $prefix_ca = substr($noid, 0, 3);
                    $sql_count = "select count(id) as jml from tbl_member_account where tipe = '$tipe' and substring(noid from 1 for 3) = '$prefix_ca'";
                    $arr_count = $db->singleRow($sql_count);
                    $prefix_subca = $prefix_ca . str_pad($arr_count->jml + 1, 4, "0", STR_PAD_LEFT);
                    $noid_add = $prefix_subca . '000000000';
                    $noid_ca = $prefix_ca . '0000000000000';
                    $noid_subca = $noid_add;
                    $hak_saldo = isset($jreq->detail->korwil) ? (int) $jreq->detail->korwil : 1;
                } elseif ($jreq->detail->tipe == 'M3' && $aturan == 'M2M3') {
                    $prefix_ca = substr($noid, 0, 3);
                    $prefix_subca = substr($noid, 0, 7);
                    $sql_count = "select count(id) as jml from tbl_member_account where tipe = '$tipe' and substring(noid from 1 for 7) = '$prefix_subca'";
                    $arr_count = $db->singleRow($sql_count);
                    $prefix_member = str_pad($arr_count->jml + 1, 9, "0", STR_PAD_LEFT);
                    $noid_add = $prefix_subca . $prefix_member;
                    $noid_ca = $prefix_ca . '0000000000000';
                    $noid_subca = $prefix_subca . '000000000';
                } else {
                    $error->regAccountTidakBerhak($saldo_member, $file_request);
                }
                if (is_numeric($nohp_email_add) !== TRUE) {
                    $tipe_message = 'EMAIL';
                } else {
                    $tipe_message = 'SMS';
                }
            } else {
                $error->regAccountJenisSalah($saldo_member, $file_request);
            }
        } else {
            $error->regAccountTidakBerhak($saldo_member, $file_request);
        }

        $username_add = substr(str_replace(' ', '', $nama_add), 0, 6) . $fungsi->randomNumber(4);
        $pass_add = $fungsi->randomString(6);

        $arr_cek = $db->cekNohpMember($nohp_email_add);

        if (!isset($arr_cek->id)) {
            if ($tipe == 'M2' || $tipe == 'M1') {
                if ($tipe == 'M1') {
                    $noid_add_m2 = $prefix_ca . '0001' . '000000000';
                } else {
                    $noid_add_m2 = $noid_add;
                }
                $nama_aktivasi = $fungsi->randomString(4);
                $db->singleRow("insert into tbl_act_card (noid,alias_act) values ('$noid_add_m2','$nama_aktivasi')");
                $msg_out = "Registrasi $jenis $tipe " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                        . "Website " . $konfig->urlWebLogin() . " USERNAME=$username_add PASSWORD=$pass_add KODE AKTIVASI LOKET $nama_aktivasi";
            } else {
                $msg_out = "Registrasi $jenis $tipe " . $konfig->namaAplikasi() . " telah berhasil. NOID:$noid_add. "
                        . "Website " . $konfig->urlWebLogin() . " USERNAME=$username_add PASSWORD=$pass_add";
            }

            $sql = "BEGIN TRANSACTION;"
                    . "insert into tbl_member_account (noid,nama,alamat,nohp_email,tipe,jenis,noid_act,noid_mit,noid_submit,status,reg_date,saldo,"
                    . "provinsi,kota_kabupaten,kecamatan,kelurahan,rt,rw,kodepos,nik,tgl_lahir,provinsi_value,kota_kabupaten_value,kecamatan_value,kelurahan_value,hak_saldo) values"
                    . "('$noid_add','$nama_add','$alamat_add','$nohp_email_add','$tipe',$jenis_tbl,'$noid','$noid_ca','$noid_subca',1,now(),0,"
                    . "'$provinsi','$kota_kabupaten','$kecamatan','$kelurahan','$rt','$rw','$kodepos','$nik_add','$tgl_lahir_add','$provinsi_value','$kota_kabupaten_value','$kecamatan_value','$kelurahan_value',$hak_saldo);"
                    . "insert into tbl_member_channel (interface,noid,alias,reg_date,last_used,passw,nama,email) values "
                    . "('WEB','$noid_add','$username_add',now(),now(),'$pass_add','$nama_add','$nohp_email_add');"
                    . "insert into log_message(nohp_email,interface,msg,secret) values ('$nohp_email_add','$tipe_message','$msg_out',1);"
                    . "COMMIT;";

            $db->singleRow($sql);

            $response = array(
                'response_code' => '0000',
                'response_message' => "Sukses Registrasi $jenis $tipe",
                'saldo' => $saldo_member
            );
        } else {
            $error->regAccountNohpEmailTerdaftar($saldo_member, $file_request);
        }
        $reply = json_encode($response);

        break;
    case "block":
        $noid_add = strtoupper(trim($jreq->detail->noid));
        $action_add = strtoupper(trim($jreq->detail->jenis));

        if ($action_add == 'BLOCK') {
            $stat = 0;
        } elseif ($action_add == 'UNBLOCK') {
            $stat = 1;
        } else {
            $error->tipeActionTidakValid($saldo_member, $file_request);
        }

        $arr = $db->cekNoidMemberNostat($noid_add);
        if (isset($arr->id)) {
            $nama_add = $arr->nama;
            $jenis_add = $arr->jenis;
            $noid_ca_add = $arr->noid_mit;
            $noid_subca_add = $arr->noid_submit;

            $tipe_add = $arr->tipe;
            if ($tipe_add == 'M1') {
                $kondisi = "noid_mit = '$noid_add'";
            } elseif ($tipe_add == 'M2') {
                $kondisi = "noid_submit = '$noid_add'";
            } elseif ($tipe_add == 'M3') {
                $error->tipeActionTidakValid($saldo_member, $file_request);
            } else {
                $kondisi = "noid = '$noid_add'";
            }

            $aturan = $jenis_member . $jenis_add; //pegawai boleh edit semua nya, M1 edit downline, M2 edit downline, M3 edit diri sendiri
            if ($aturan == '21' || $noid == $noid_ca_add || $noid == $noid_subca_add) {

                $sql = "BEGIN TRANSACTION;"
                        . "update tbl_member_account set status = $stat "
                        . "where $kondisi;"
                        . "COMMIT;";
                $db->singleRow($sql);

                $response = array(
                    'response_code' => '0000',
                    'response_message' => "$action_add MEMBER $nama_add DATA BERHASIL",
                    'saldo' => $saldo_member
                );
            } else {
                $error->regAccountTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->accountTidakAda($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
    case "check":
        $dest_add = strtoupper($jreq->detail->noid);
        $arr = $db->cekNoidMember($dest_add);
        if (isset($arr->id)) {
            $response = array(
                'response_code' => '0000',
                'response_message' => 'CEK DATA MEMBER BERHASIL',
                'nama' => $arr->nama,
                'nik' => $arr->nik,
                'tgl_lahir' => $arr->tgl_lahir,
                'nohp_email' => $arr->nohp_email,
                'alamat' => $arr->alamat,
                'provinsi' => $arr->provinsi,
                'provinsi_value' => $arr->provinsi_value,
                'kota_kabupaten' => $arr->kota_kabupaten,
                'kota_kabupaten_value' => $arr->kota_kabupaten_value,
                'kecamatan' => $arr->kecamatan,
                'kecamatan_value' => $arr->kecamatan_value,
                'kelurahan' => $arr->kelurahan,
                'kelurahan_value' => $arr->kelurahan_value,
                'rt' => $arr->rt,
                'rw' => $arr->rw,
                'kodepos' => $arr->kodepos
            );
        } else {
            $error->accountTidakAda($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
    case "delete":
        $noid_add = strtoupper(trim($jreq->detail->noid));
        $arr = $db->cekNoidMember($noid_add);
        if (isset($arr->id)) {
            $jenis_add = $arr->jenis;
            //kirim message
            if ($arr->saldo == 0) {
                $msg_out = "Akun $noid_add a.n $arr->nama TELAH DIHAPUS";
                $db->kirimMessage($noid_add, $msg_out);
                $sql = "BEGIN TRANSACTION;"
                        . "delete from tbl_member_channel where noid = '$noid_add';"
                        . "update tbl_member_account set nohp_email = 'DELETE $ref', status = 0, jenis = 0 where noid = '$noid_add';"
                        . "COMMIT;";
                $db->singleRow($sql);

                $response = array(
                    'response_code' => '0000',
                    'response_message' => 'DELETE DATA BERHASIL',
                    'saldo' => $saldo_member
                );
            } else {
                $error->globalTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->accountTidakAda($saldo_member, $file_request);
        }
        $reply = json_encode($response);
        break;
    case "edit":
        //edit nama email
        $noid_add = strtoupper(trim($jreq->detail->noid));
        $nama_add = strtoupper(trim($jreq->detail->nama));
        $nik_add = strtoupper(trim($jreq->detail->nik));
        $tgl_lahir_add = strtoupper(trim($jreq->detail->tgl_lahir));
        $alamat_add = strtoupper(trim($jreq->detail->alamat));
        $provinsi = strtoupper($jreq->detail->provinsi);
        $provinsi_value = strtoupper($jreq->detail->provinsi_value);
        $kota_kabupaten = strtoupper($jreq->detail->kota_kabupaten);
        $kota_kabupaten_value = strtoupper($jreq->detail->kota_kabupaten_value);
        $kecamatan = strtoupper($jreq->detail->kecamatan);
        $kecamatan_value = strtoupper($jreq->detail->kecamatan_value);
        $kelurahan = strtoupper($jreq->detail->kelurahan);
        $kelurahan_value = strtoupper($jreq->detail->kelurahan_value);
        $rt = strtoupper($jreq->detail->rt);
        $rw = strtoupper($jreq->detail->rw);
        $kodepos = strtoupper($jreq->detail->kodepos);

        $arr = $db->cekNoidMember($noid_add);
        if (isset($arr->id)) {
            $jenis_add = $arr->jenis;
            $noid_ca_add = $arr->noid_mit;
            $noid_subca_add = $arr->noid_submit;

            $aturan = $jenis_member . $jenis_add; //pegawai boleh edit semua nya, M1 edit downline, M2 edit downline, M3 edit diri sendiri
            if ($aturan == '21' || $noid == $noid_add || $noid == $noid_ca_add || $noid == $noid_subca_add) {

                $sql = "BEGIN TRANSACTION;"
                        . "update tbl_member_account set nama = '$nama_add', alamat = '$alamat_add', nik = '$nik_add', "
                        . "tgl_lahir = '$tgl_lahir_add', provinsi = '$provinsi', kota_kabupaten = '$kota_kabupaten', "
                        . "kecamatan = '$kecamatan', kelurahan = '$kelurahan', provinsi_value = '$provinsi_value', kota_kabupaten_value = '$kota_kabupaten_value', "
                        . "kecamatan_value = '$kecamatan_value', kelurahan_value = '$kelurahan_value', rt = '$rt', rw = '$rw', kodepos = '$kodepos' "
                        . "where noid = '$noid_add';"
                        . "COMMIT;";
                $db->singleRow($sql);

                $response = array(
                    'response_code' => '0000',
                    'response_message' => 'UPDATE DATA BERHASIL',
                    'saldo' => $saldo_member
                );
            } else {
                $error->regAccountTidakBerhak($saldo_member, $file_request);
            }
        } else {
            $error->accountTidakAda($saldo_member, $file_request);
        }

        $reply = json_encode($response);
        break;
}
