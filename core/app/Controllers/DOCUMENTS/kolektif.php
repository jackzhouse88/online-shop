<?php

$fungsi = new Libraries\Fungsi();
$db = new Models\Databases();
$id_group = $arr_jenis[1];
$content = '';

$sql_cek = "select id,idpel,idpel_name,tagihan,admin_bank,total_tagihan,bulan from tbl_group_kolektif_detail where id_group = $id_group;";
$arr = $db->multipleRow($sql_cek);

if (isset($arr[0]->id)) {
    if ($printType == 'DOTMATRIX') {
        $contentTmp = $fungsi->spasi(4, 'NO').'| '
                . $fungsi->spasi(18, 'IDPEL').' | '
                . $fungsi->spasi(30, 'NAMA PELANGGAN').' | '
                . $fungsi->spasi(14, '   TAGIHAN').' | '
                . $fungsi->spasi(14, '  ADMIN BANK').' | '
                . $fungsi->spasi(14, ' TOTAL TAGIHAN').' |'
                . $fungsi->spasiKanan(3, 'LBR') . '
';
        ;
        $i = 1;
        //no idpel idpel_name tagihan admin_bank total tagihan lembar
        foreach ($arr as $isi) {
            $contentTmp .= $fungsi->spasi(4, $i).'| '
                    . $fungsi->spasi(18, $isi->idpel).' | '
                    . $fungsi->spasi(30, $isi->idpel_name).' | '
                    . $fungsi->spasiKanan(14, $fungsi->rupiah($isi->tagihan)).' | '
                    . $fungsi->spasiKanan(14, $fungsi->rupiah($isi->admin_bank)).' | '
                    . $fungsi->spasiKanan(14, $fungsi->rupiah($isi->total_tagihan)).' |'
                    . $fungsi->spasiKanan(3, $isi->bulan) . '
';
            $i++;
        }
        $content = $contentTmp;
    } else {
        
    }
} else {
    
}
