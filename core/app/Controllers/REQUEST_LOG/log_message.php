<?php

switch ($action_request) {
    case "add":
        $message_add = strtoupper($jreq->detail->message);

        switch ($tipe_member) {
            case 'M1':
                $sql = "insert into log_message(waktu,noid,nohp_email,interface,msg,reff,stat,m1) values "
                        . "(now(),'$noid','$noid','BROADCAST','$message_add','$ref',1,'$noid')";
                break;
            case 'M2':
                $sql = "insert into log_message(waktu,noid,nohp_email,interface,msg,reff,stat,m2) values "
                        . "(now(),'$noid','$noid','BROADCAST','$message_add','$ref',1,'$noid')";
                break;
            case 'M3':
                $error->globalTidakBerhak($saldo_member, $file_request);
                break;
            default :
                $sql = "insert into log_message(waktu,noid,nohp_email,interface,msg,reff,stat,pusat) values "
                        . "(now(),'$noid','$noid','BROADCAST','$message_add','$ref',1,1)";
                break;
        }

        $db->singleRow($sql);
        $response = array(
            'response_code' => '0000',
            'response_message' => "Broadcast pesan berhasil",
            'saldo' => $saldo_member
        );
        $reply = json_encode($response);
        break;
    case "delete":

        $id_add = strtoupper($jreq->detail->id);

        $sql_cek = "select id from log_message where id = $id_add;";

        $arr_cek = $db->singleRow($sql_cek);

        if (isset($arr_cek->id)) {
            $sql_delete = "delete from log_message where id = $id_add;";
            $db->singleRow($sql_delete);
            $response = array(
                'response_code' => '0000',
                'response_message' => "Hapus Pesan Berhasil.",
                'saldo' => $saldo_member
            );
        } else {
            $error->dataTidakAda($saldo_member, $file_request);
        }
        $reply = json_encode($response);
        break;
    case "delete_all":
        $sql_delete = "delete from log_message where noid = '$noid' and stat = 1;";
        $db->singleRow($sql_delete);
        $response = array(
            'response_code' => '0000',
            'response_message' => "Hapus Seluruh Pesan Berhasil.",
            'saldo' => $saldo_member
        );

        $reply = json_encode($response);
        break;
    case "resend":

        $id_add = strtoupper($jreq->detail->id);

        $sql_cek = "select id from log_message where id = $id_add;";

        $arr_cek = $db->singleRow($sql_cek);

        if (isset($arr_cek->id)) {
            $sql_delete = "update log_message set stat = 0, resend = resend + 1 where id = $arr_cek->id;";
            $db->singleRow($sql_delete);
            $response = array(
                'response_code' => '0000',
                'response_message' => "Kirim ulang message berhasil",
                'saldo' => $saldo_member
            );
        } else {
            $error->dataTidakAda($saldo_member, $file_request);
        }
        $reply = json_encode($response);
        break;
}