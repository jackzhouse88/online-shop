<?php

$dbVa = new Models\DbVa();

$nomor_va = trim($jreq->detail->nomor_va);
$action = $jreq->detail->action; //inq exec
$noid_add = $jreq->detail->noid;
$amount_add = $jreq->detail->nominal;
$keterangan_add = strtoupper($jreq->detail->keterangan);
$tujuan_add = strtoupper($jreq->detail->tujuan);

$arr_cek_va = $dbVa->cekNomorVaBnis($nomor_va);

if (!isset($arr_cek_va->id)) {
    $error->accountTidakAda($saldo_member,$file_request);
}
if ($arr_cek_va->noid != $noid_add) {
    $error->accountTidakAda($saldo_member,$file_request);
}

if ($arr_cek_va->jenis == 0) { //open payment atau top up saldo
    //top up saldo member
    $arr_cek_mbr = $db->cekNoidMember($noid_add);

    if (!isset($arr_cek_mbr->id)) {
        $error->accountTidakAda($saldo_member,$file_request);
    }
    $noid_add = $arr_cek_mbr->noid;
    $nama_add = $arr_cek_mbr->nama;
    $tipe_add = $arr_cek_mbr->tipe;
    $nohp_email_add = $arr_cek_mbr->nohp_email;

    if ($noid_add == $noid) {
        $error->accountTidakValid($saldo_member,$file_request);
    }

    if ($amount_add < 100) {
        $error->minimalTransaksi($saldo_member,$file_request);
    }

    $aturan = $tipe_member . $tipe_add;

    if ($aturan == 'ADMINM1' || $aturan == 'ADMINM2' || $aturan == 'ADMINM3') {


        if (($saldo_member < $amount_add) && $tipe_member != 'ADMIN') {
            $error->tipeActionTidakValid($saldo_member,$file_request);
        } else {
            $amount_minus = $amount_add * -1;
            $data_trx = array(
                'username' => $username,
                'interface' => $interface,
                'product' => 'TOPUP',
                'product_detail' => 'SALDO',
                'idpel' => $noid_add,
                'idpel_name' => $nama_add,
                'amount' => $amount_add,
                'keterangan' => $keterangan_add,
                'bank' => $tujuan_add,
                'reff' => $ref,
                'trace_id' => $ref,
                'lembar' => 1,
                'response_code' => '0000',
                'response_message' => 'sukses'
            );
            $jlast_trx = json_encode($data_trx);
            if ($tipe_member == 'ADMIN') {
                $saldo_last = 0; //karena ADMIN saldo unlimit
                //sql_insert manual
                $sql_insert_manual = "insert into log_data_trx (waktu,noid,reff,amount,saldo,detail) "
                        . "values (now(),'$noid','$ref','$amount_minus','$saldo_last','$jlast_trx');";
                $db->singleRow($sql_insert_manual);
            } else {
                $sql_kurang_saldo = "update tbl_member_account set saldo = saldo + $amount_minus,last_trx='$jlast_trx',last_amount='$amount_minus',last_reff='$ref',last_fee='{}' where noid = '$noid' returning saldo;";
                $saldo_noid = $db->singleRow($sql_kurang_saldo);
                $saldo_last = $saldo_noid->saldo;
            }
            $data_trx_add = array(
                'username' => $nama_member,
                'interface' => $interface,
                'product' => 'TOPUP',
                'product_detail' => 'SALDO',
                'idpel' => $noid,
                'idpel_name' => $nama_member,
                'amount' => $amount_add,
                'keterangan' => $keterangan_add,
                'bank' => $tujuan_add,
                'reff' => $ref,
                'trace_id' => $ref,
                'lembar' => 1,
                'response_code' => '0000',
                'response_message' => 'sukses'
            );
            $jlast_trx_noid = json_encode($data_trx_add);
            $sql_tambah_saldo = "update tbl_member_account set saldo = saldo + $amount_add,last_trx='$jlast_trx_noid',last_amount=$amount_add,last_reff='$ref',last_fee='{}' where noid = '$noid_add' returning saldo;";
            $saldo_add = $db->singleRow($sql_tambah_saldo);
            $saldo_add_last = $saldo_add->saldo;

            $response = array(
                'response_code' => '0000',
                'saldo' => $saldo_last,
                'response_message' => "TOPUP SALDO KE $noid_add a.n $nama_add Rp. " . $fungsi->rupiah($amount_add) . " BERHASIL. REFF : $ref",
                'trace_id' => $ref
            );

            $msg_out = $konfig->namaAplikasi() . ", TOPUP SALDO MELALUI VIRTUAL ACCOUNT BNI $nomor_va Rp. " . $fungsi->rupiah($amount_add) . " BERHASIL REFF $ref. Saldo Rp. " . $fungsi->rupiah($saldo_add_last);
            $db->kirimMessage($noid_add, $msg_out);
        }
    } else {
        $error->accountTidakValid($saldo_member,$file_request);
    }
} else {
    //1 pembayaran tagihan
    $arr_cek_tgh = $dbVa->cekIdTagihan($noid_add);
    if (!isset($arr_cek_tgh->id)) {
        $error->accountTidakAda($saldo_member,$file_request);
    }
    //update tabel tagihan
    $noid_creator = $arr_cek_tgh->noid;
    //1 ada masuk, 2 lunas, 3 kelebihan
    if ($arr_cek_tgh->total_tagihan == $amount_add) {
        $status_pay = 2;
    } else {
        $status_pay = 1;
    }
    $sql_update = "update tbl_tagihan set saldo = saldo + $amount_add, waktu_bayar = now(), vsn = '$ref', status = $status_pay "
            . "where id=$arr_cek_tgh->id;";
    $dbVa->singleRow($sql_update);

    $response = array(
        'response_code' => '0000',
        'saldo' => $saldo_member,
        'response_message' => "Pembayaran Tagihan $arr_cek_tgh->jenis_tagihan $noid_add dengan Nomor VA $nomor_va Rp " . $fungsi->rupiah($amount_add) . " BERHASIL. REFF : $ref"
    );
    $db->kirimMessage($arr_cek_tgh->noid, "Pembayaran Tagihan $arr_cek_tgh->jenis_tagihan $noid_add dengan Nomor VA $nomor_va Rp " . $fungsi->rupiah($amount_add) . " BERHASIL. REFF : $ref");
}
$reply = json_encode($response);
