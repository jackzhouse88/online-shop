<?php

namespace Controllers;

use Resources,
	Libraries,
	Models;

class PRINTS extends Resources\Controller {

	public function __construct() {
		parent::__construct();
	}

	public function REQUEST($printType, $traceId) {
		$arr_data = explode('_', $traceId);
		foreach ($arr_data as $data) {
			if (is_numeric($data)) {
				echo $this->PREVIEW($printType, $data);
			} else {
				echo $this->DOCUMENTS($printType, $data);
			}
		}
	}

	public function PREVIEW($printType, $traceId) {
		$db = new Models\Databases();
		$sql = "update log_detail_trx set cetak = cetak + 1 where reff = '$traceId';"
				. "select noid,waktu::timestamp(0),cetak,struk,product,product_detail from log_detail_trx where reff = '$traceId' order by id desc limit 1";
		$arr_trx = $db->singleRow($sql);
		$content = '';
		if (isset($arr_trx->noid)) {
			if ($printType == 'DOTMATRIX') {
				$sql_tambah = "update tbl_member_account set printing = printing * -1 where noid = '$arr_trx->noid' returning printing;";
				$arr_tambah = $db->singleRow($sql_tambah);
				$tambah = $arr_tambah->printing;
				$tambahan = '';
				if ($tambah == 1) {
					$tambahan = '
0x0';
				} else {
					$tambahan = '';
				}
			}
			if ($arr_trx->product == 'PULSA') {
				include 'PRINTS/PULSA.php';
			} else {
				$operator = $arr_trx->product . '_' . $arr_trx->product_detail;
				include 'PRINTS/' . $operator . '.php';
			}
		} else {
			$content = "DATA TIDAK DITEMUKAN";
		}
		return $content;
	}

	public function DOCUMENTS($printType, $data) {
		//data = kolektif*4
		$arr_jenis = explode('-', preg_replace('/[^a-zA-Z0-9\-\_\#\@\ \.\,\:\"\]\[\}\{]/', '', $data));
		$jenis = $arr_jenis[0];
		include "DOCUMENTS/$jenis.php";
		return $content;
	}

}
