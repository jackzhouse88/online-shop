<?php

namespace Libraries;
use Resources;

class ResponseError {

    public static function invalidSession($file_request) {
        $response = array('response_code' => '0111','response_message' => 'Session waktu telah habis, silahkan login kembali.');
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function invalidIp($file_request) {
        $response = array('response_code' => '0111','response_message' => 'IP tidak sesuai');
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function kesalahanSistem($tipe_request,$file_request) {
        $response = array('response_code' => '0122','response_message' => 'Terjadi Kesalahan Sistem pada : '.$tipe_request);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function menuTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Menu tidak tersedia','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function menuTidakBerhak($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Anda tidak berhak mengakses menu ini','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function requestTerlaluCepat($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Request terlalu cepat','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function accountTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Account tidak ditemukan atau sedang terblokir','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function channelTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Channel tidak ditemukan','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function dataTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Data tidak ditemukan','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function dataCashOutTidakAda($id_nfc,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Data tidak ditemukan','nfc_id' => $id_nfc);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function accountTidakValid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Account tujuan tidak valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function regAccountNohpEmailTerdaftar($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Gagal, Nohp atau Email telah Terdaftar','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    //channel alias terdaftar
    public static function regAccountAliasTerdaftar($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Ulangi dan Periksa penambahan Channel Transaksi, Alias telah Terdaftar','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function regAccountHarusEmail($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Registrasi Gagal, harus menggunakan Email','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function regAccountJenisSalah($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Registrasi Gagal, Jenis harus PEGAWAI atau MEMBER','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function regAccountTidakBerhak($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Registrasi Gagal, Anda tidak berhak','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function globalTidakBerhak($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Anda tidak berhak','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function otorisasiButuh($saldo,$file_request) {
        $response = array('response_code' => '0000','response_message' => 'Request Diterima. Permintaan anda telah masuk ke sistem otorisasi','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function otorisasiSudahAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Request Ditolak karena Double, sudah ada permintaan yang sama','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function otorisasiTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Otorisasi Gagal, Kode Validasi yang anda masukkan salah','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function minimalTransaksi($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Transaksi Gagal, nominal minimal Rp. 500','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function saldoTidakCukup($saldo,$file_request) {
        $response = array('response_code' => '0510','response_message' => 'Transaksi Gagal, saldo anda tidak cukup. Silahkan TopUp Saldo kembali','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function saldoTargetTidakCukup($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Transaksi Gagal, Saldo Member tidak cukup untuk Cash Out atau Pembayaran.','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function tipeActionTidakValid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Request Gagal, tipe action tidak valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function gantiPasswordTidakValid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Password Baru tidak valid, gunakan angka dan huruf saja','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function gantiPasswordBaruTidakSama($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Password Baru tidak sama, periksa inputan password baru anda','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function gantiPasswordLamaTidakSama($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Password Lama Salah, periksa inputan password lama anda','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function productTidakAda($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Produk tidak tersedia atau sedang ditutup','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function plnPrepaidNominal($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'PLN PREPAID, Pilih nominal','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function productDitutup($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Produk sedang ditutup. Hubungi CS untuk informasi','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function requestPayTrxInvalid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Request Transaksi tidak valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function requestPayTrxInvalidCredential($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Credential Request Transaksi tidak valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function regIdTagihanTerdaftar($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'ID Tagihan telah terdaftar','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function tagihanTidakDitemukan($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'ID Tagihan tidak ada','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function adminBankTidakValid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Transaksi Gagal, Biaya Admin Bank Tidak Valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function namaKolektifTerdaftar($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Input Group Kolektif Gagal, Nama telah terdaftar','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function idpelKolektifTerdaftar($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Proses Input Group Kolektif Gagal, IDPEL telah terdaftar','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function idpelTidakValid($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'IDPEL tidak valid','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function saldoTarikTidakCukup($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Saldo Loket yang akan ditarik tidak mencukupi','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }
    
    public static function resetPasswordGagal($saldo,$file_request) {
        $response = array('response_code' => '0501','response_message' => 'Reset Password Gagal, Hanya interface WEB yang bisa di reset.','saldo' => $saldo);
        $wLog = new Libraries\WriteLog;
        $wLog->writeLog($file_request, $response);
        die(json_encode($response));
    }

}
