<?php

$password = 'admin';

return array(

    'default' => array(
        'driver' => 'pgsql',
        'host' => 'localhost',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'basic',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'alamat' => array(
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'alamat',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'xbilling' => array(
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'xbilling',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'xbmt' => array(
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'xbmt',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'xhrd' => array(
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'xhrd',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'xposo' => array(
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'xposo',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    ),

    'ejabberd' => array(
        'driver' => 'pgsql',
        'host' => 'localhost',
        'port' => 5432,
        'user' => 'postgres',
        'password' => $password,
        'database' => 'ejabberd',
        'tablePrefix' => '',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'pdo' => false
    )

);
