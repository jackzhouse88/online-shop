2018-04-01
db: 
tbl_product_map
    provider character varying(20),
    hpp integer DEFAULT 0,
    admin_bank integer DEFAULT 0,
    margin smallint DEFAULT 0,

tbl_member_account
    fee jsonb DEFAULT '{}'::jsonb,
    fee_dist smallint DEFAULT 0,
    hak_saldo smallint DEFAULT 0,

log_konfirmasi_topup 
    norek_tujuan character varying(50),

log_message
    notif smallint DEFAULT 0,


tambahkan data menu
insert into tbl_menu (perintah) values ('tbl_member_channel_block');
insert into tbl_menu (perintah) values ('menu_edit_setting_korwil');
insert into tbl_menu (perintah) values ('menu_edit_setting_ca');
insert into tbl_menu (perintah) values ('rpaging_distribusi_fee_m1');
insert into tbl_menu (perintah) values ('rpaging_distribusi_fee_m2');
insert into tbl_menu (perintah) values ('rpaging_distribusi_fee_m3');
insert into tbl_menu (perintah) values ('menu_ganti_nohp_email');

