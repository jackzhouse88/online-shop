<?php
//ini_set('error_reporting', E_ALL  & ~E_NOTICE);
ini_set('display_errors', 0);
include_once '../mainconfig.php';

$id_trx = filter_input(INPUT_GET, 'id');
$tipe = 'INK';

$url = MAIN_URL.'framework/core/public/index.php/PRINTS/REQUEST/PDF/'.$id_trx;
$content = file_get_contents($url);

if($content != ''){
    if($tipe == 'HTML'){
        $content;
    }elseif($tipe == 'INK'){
        require_once('html2pdf.class.php');
        try
        {        
            $html2pdf = new HTML2PDF('P', 'A4','en');
            $html2pdf->pdf->SetDisplayMode('fullpage');
            $html2pdf->writeHTML($content);
            $html2pdf->Output($id_trx.'.pdf');
        }
        catch(HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }
    print $content;
}else{
    //data tidak ada
}